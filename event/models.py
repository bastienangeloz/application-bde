from django.db import models
# from django.contrib.auth.models import User
import uuid


class TagsEvent(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Note(models.Model):
    note = models.FloatField()

    def __str__(self):
        return self.note

class Event(models.Model):
    ref = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    id_event = models.CharField(max_length=255)
    date_event = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255)
    description = models.TextField()
    tags_event = models.ManyToManyField(TagsEvent)
    note = models.ManyToManyField(Note)
    is_finished = models.BooleanField(default=False)
    # participants = models.ManyToManyField(User, related_name='participants')

