"""
Ce module présente les points de terminaison des urls pour le frontend. Nous pouvons egalement les retrouver dans
la construction de l'application et le dossier src/App.js. ce sont les routes de navigation configurées.

.. note:: Points de terminaison du frontend

    .. code-block:: python

        # frontend navigation pages principales
        path('', views.index),
        path('home', views.index),

"""

from django.urls import path
from . import views

urlpatterns = [
    # frontend navigation pages principales
    path('', views.index),
]
