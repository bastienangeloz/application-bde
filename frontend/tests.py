"""
Ce module presente tous les tests effectuer pour le package frontend
"""

from django.test import (
    Client,
    TestCase
)


class IndexViewTestCase(TestCase):
    """
    Class de test pour le rendu html du frontend et lien avec React
    """
    def setUp(self):
        self.client = Client()

    def test_index_view(self) -> None:
        """
        Test pur le rendu de la view générale

        :return: Réussite ou non du test
        """
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'frontend/index.html')
