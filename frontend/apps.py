"""
Ce module permet de charger l'application frontend dans le projet general
"""

from django.apps import AppConfig


class FrontendConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'frontend'
