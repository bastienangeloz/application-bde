// contexte
import * as React from 'react'
import { connect } from 'react-redux';
import Main from '../../layouts/Main';


const HomePage = ({isAuthenticated, info_user}) => {

  console.log(info_user)

  React.useEffect(() => {
    console.log("Welcome home")
  },[])
  
  return (
    <Main>
      <div className='homepage'>
        {info_user}
      </div>
    </Main>
  );
}
  
const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  info_user: state.auth.info_user
});

export default connect(mapStateToProps)(HomePage);