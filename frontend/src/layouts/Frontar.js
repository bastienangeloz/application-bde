// context
import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
// components
import {IoIosFolder, IoIosStats, IoMdPerson} from 'react-icons/io'
// block
import Hamburger from './Hamburger'

const Frontar = ({info_user}) => {

    const routes = [
        {
            index: true,
            label: 'MyBDE',
            path: '/',
            firstclass: null,
            scdclass: null,
            icon: null,
          },
          {
            label: 'Actualité',
            link: true,
            otherpath: null,
            path: '/actus',
            firstclass: 'base-icon',
            scdclass: 'change-color-icon',
            icon: <IoIosStats size={26} className='iconspe' style={{cursor:'pointer'}}/>,
          },
          {
            label: 'Evenements',
            link: true,
            otherpath: null,
            path: '/events',
            firstclass: 'base-icon',
            scdclass: 'change-color-icon',
            icon: <IoIosFolder size={26} className='iconspe' style={{cursor:'pointer'}}/>,
          },
          {
            label: 'Profil',
            link: true,
            otherpath: null,
            path: '/profil',
            firstclass: null,
            scdclass: 'menu-icon',
            icon: <IoMdPerson size={26} className='icon' style={{cursor:'pointer'}}/>,
          },
    ]

    React.useEffect(() => {
        console.log('info_user')
    }, [info_user])

  return (
    <div id="header">
        <h1 className="index-link">
        {routes.filter((l) => l.index).map((l) => (
            <Link key={l.label} to={l.path}><span className='index-titre'>{l.label}</span></Link>
        ))}
        </h1>
        <nav className="links">
        <div style={{display:'flex', justifyContent:'space-between', paddingRight:'2%'}}>
        <ul>
            {routes.filter((l) => !l.index).slice(0,2).map((l) => (
            <li key={l.label}>
                <div className={l.firstclass}>
                    <div className={l.scdclass}>
                        {l.link ?
                        <Link to={l.path}>
                            <h6 className="index-link"><span className='index-titre'>{l.label}</span></h6>
                        </Link>
                        :
                        <a href={l.otherpath} target="_blank" rel="noreferrer">
                            <h6 className="index-link"><span className='index-titre'>{l.label}</span></h6>
                        </a>
                        }
                    </div>
                </div>
            </li>
            ))}
        </ul>
        <ul>
            {routes.filter((l) => !l.index).slice(2).map((l) => (
            <li key={l.label}>
                <div className={l.firstclass}>
                    <div className={l.scdclass}>
                        {l.link ?
                        <Link to={l.path}>
                            <h6 className="index-link"><span className='index-titre'>{l.label}</span></h6>
                        </Link>
                        :
                        <a href={l.otherpath} target="_blank" rel="noreferrer">
                            <h6 className="index-link"><span className='index-titre'>{l.label}</span></h6>
                        </a>
                        }
                    </div>
                </div>
            </li>
            ))}
        </ul>
        </div>
        </nav>
        <Hamburger />
    </div>
  );
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    info_user: state.auth.info_user
});

export default connect(mapStateToProps, {})(Frontar);