// context
import React, { Suspense, lazy, useState } from 'react';
import { Link } from 'react-router-dom';
// components
import { IoIosColorFilter, IoIosStats, IoMdPerson } from 'react-icons/io'

// import react hamburger lazy
const Menu = lazy(() => import('react-burger-menu/lib/menus/slide'));

const Hamburger = () => {
  const [open, setOpen] = useState(false);

  const routes = [
    {
        index: true,
        label: 'MyBDE',
        path: '/',
        firstclass: null,
        scdclass: null,
        icon: null,
      },
      {
        label: 'Actualités',
        link: true,
        otherpath: null,
        path: '/actus',
        firstclass: 'base-icon',
        scdclass: 'change-color-icon',
        icon: <IoIosStats size={26} className='iconspe' style={{cursor:'pointer'}}/>,
      },
      {
        label: 'Evenements',
        link: true,
        otherpath: null,
        path: '/events',
        firstclass: 'base-icon',
        scdclass: 'change-color-icon',
        icon: <IoIosColorFilter size={26} className='iconspe' style={{cursor:'pointer'}}/>,
      },
      {
        label: 'Profil',
        link: true,
        otherpath: null,
        path: '/profil',
        firstclass: null,
        scdclass: 'menu-icon',
        icon: <IoMdPerson size={26} className='icon' style={{cursor:'pointer'}}/>,
      },
]

  return (
    <div className="hamburger-container">
      <nav className="main" id="hambuger-nav">
        <ul>
          {open ? (
            <li className="menu close-menu">
              <div onClick={() => setOpen(!open)} className="menu-hover">&#10005;</div>
            </li>
          ) : (
            <li className="menu open-menu">
              <div onClick={() => setOpen(!open)} className="menu-hover">&#9776;</div>
            </li>
          )}
        </ul>
      </nav>
      <Suspense fallback={<></>}>
        <Menu right isOpen={open} disableAutoFocus>
          <ul className="hamburger-ul">
          {routes.filter((l) => !l.index).map((l) => (
            <li key={l.label}>
                <div className={l.firstclass}>
                    <div className={l.scdclass}>
                        {l.link ?
                        <Link to={l.path} onClick={() => setOpen(!open)}>
                          <div style={{display:'flex', justifyContent:'flex-start'}} >
                            <h6 className="index-link"><span className='index-titre'>{l.label}</span></h6>
                          </div>  
                        </Link>
                        :
                        <a href={l.otherpath} target="_blank" rel="noreferrer" onClick={() => setOpen(!open)}>
                          <div style={{display:'flex', justifyContent:'flex-start'}} >
                            <h6 className="index-link"><span className='index-titre'>{l.label}</span></h6>
                          </div>
                        </a>
                        }
                    </div>
                </div>
            </li>
            ))}
          </ul>
        </Menu>
      </Suspense>
    </div>
  );
};

export default Hamburger;
