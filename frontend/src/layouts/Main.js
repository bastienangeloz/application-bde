// context
import React from 'react';
import PropTypes from 'prop-types';
// block
import Sidebar from './Frontar';

const Main = (props) => (
    <div id="wrapper">
      <Sidebar />
      <div id="main">
        {props.children}
      </div>
    </div>
);

Main.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  fullPage: PropTypes.bool,
  title: PropTypes.string,
  description: PropTypes.string,
};

Main.defaultProps = {
  children: null,
  fullPage: false,
  title: null,
  description: "theLab",
};

export default Main;
