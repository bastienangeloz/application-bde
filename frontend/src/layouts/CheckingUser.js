// context
import * as React from 'react';
import { connect } from 'react-redux';
import { checkAuthenticated, load_info } from '../reducers/action_auth';

const CheckUser = ({ checkAuthenticated, load_info, children }) => {
    React.useEffect(() => {
        checkAuthenticated();
        load_info();
    }, []);

    return (
        <div>
            {children}
        </div>
    );
};

export default connect(null, { checkAuthenticated, load_info })(CheckUser);