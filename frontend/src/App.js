// style
import './styles/main.scss';
// routes, store et user
import * as React from 'react'
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import CheckingUser from './layouts/CheckingUser';
// page principale
import HomePage from './pages/principales/Home';

function App() {

  return (
    <Provider store={store}>
      <BrowserRouter>
        <CheckingUser/>
          <Routes>
            <Route path="/" element={<HomePage />}/>
          </Routes>
        <CheckingUser/>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
