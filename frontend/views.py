"""
Ce module présente les views pour le package frontend
"""

from django.http import HttpResponse, HttpRequest
from django.shortcuts import render
from django.views.decorators.http import require_safe


@require_safe
def index(request: HttpRequest, *args, **kwargs) -> HttpResponse:
    """
    View de rendu général du frontend qui charge l'index.html comprenant tout le reste du frontend

    :param request: Requete web
    :type request: HttpRequest
    :return: Rendu du template de l'index.html
    :rtype: HttpResponse
    """
    return render(request, 'frontend/index.html')
