"""
Ce package contient les modules necessaire pour la gestion du frontend.

Fonctionnement
_______________

le modul frontend englobe toute la partie frontend avec une application React.js. Il permet egalement de communiqué
via le backend avec les url via le fichier **urls.py**. Ci-dessous, un recapitulatif des differentes sections du
module frontend et ce qu'elles font de particulier. Pour des raisons de clartés, seule les parties suceptibles d'être
compliqué seront détaillées.

Lancement du compilateur
_________________________

Pour compiler les fichiers JS/CSS/HTML du front se placer dans le dossier ./ialab/frontend et lancer la commande:

.. code-block:: python

    npm run dev


Webpack compilera les fichiers et sauvegardera un fichier static present dans .frontend/static/frontend/main.js A
chaue changement dans les fichiers frontend, le compilateur se redeclenche pour recompiler et favorisr l'experience
de developpement.

Recupération du frontend par le Backend
________________________________________

La views présente dans le fichier **views.py** viendra recupérer un fichier **index.html** présent dans le dossier
.**./frontend/templates/frontend/** (nomenclature détecté par django). Ce fichier fait reference aux fichiers static
compilé main.js eainsi que le css dans le dossier ;/frontend/static qui est mis à jour à chaque compilation du front.
La mise à jour est donc recupérer à chaue compilation et assimilé par les routes du backend.

URLS
______

Pour permettre la navigation entre différentes pages, le systeme de navigation doit être à jour coté front (fichier
App.js) mais aussi coté back avec le fichier **urls.py** présent dans le dossier frontend

Webpack
_________

Le fichier webpack.config.js est le fichier qui presente les reglages pour le compilateur. Il peut être modifié si
notre compilation necessite des besoins specifique

Dependances
____________

Les dependances frontend sont présentes dans le fichier de configuration **package.json**.
Pour configurer les dependance ainsi que les reglages de base, effectuer cette commande dans le dossier frontend

.. code-block:: python

    npm install

Cela installera notamment les nodes_modules necessaires à l'application.

Bruit blanc _____________ la dépendance **whitenoise** à été installé coté serveur (python) pour resoudre le probleme
suivant. Certains dossiers nommés `static` etait en conflit lros de l'initialisation des credentials GOOGLE CLOUD.
Cela destructurait totalement le rendu frontend et admin. Cette dépendance à parmis de resoudre le probleme avec des
reglages effectués dans **ialab/settings.py**

Dossier source (SRC)
_____________________

Le dossier source contient tout le code necessaire aux fonctionnements du front end. Les sections suivantes sont des
sections qui presentes certaines fonctionnalités (difficile à comprendre sans documentation) presentes dans le front
end.

Fonctionalités
________________

Les fonctionnalités suivantes sont présentées dans le but d'obtenir des informartions sur la maintenabilité et
l'execution des composants.

Composant CSV READER
----------------------

Le composant CSVReader est le composant permettant de parser le CSV uploader par le client et le traiter.
Il est issu de la bibliotheque PapaParse, la documentation se trouve ici : https://www.papaparse.com/docs

Au dela du parsing CSV -> JSON, il applique plusieurs transformation:
    - header: true = utiliser les premières ligne comme en-tête
    - dynamicTyping: true = type les variables selon le type de donnée (qualitative/numerique)
    - skipEmptyLines: "greedy" = supprime les lignes vides **ET** la dernière ligne vide d'un csv si elle est presente
    - transformHeader: function() = fonction de transformation qui transforme les noms de colonne contenant des **"."** en "**_"** si necessaire. cette transformation est utilse car AggridTabble ne reconnait pas les colonne avec des **"."**.
    - complete: function() = fonction appliqué **APRES** le parsing des données. Ici, une fonction est appliquée pour transformation les données avec des **","** en **"."** et transformer les valeurs en float. Cela permet de typer les variables en numerique pour les données arrivant avec des virgules.

Avec plusieurs fonctions, nous obtenons ainsi une structure de différentes varibale (quantitative et qualitative) que
nous passons en options groupées au selecteur mutliple pour permettre de proposer une selection par type de variables.

Store Redux
____________

Le maintien des etat des utilisateurs dans le frontend est effectué par Redux. Plusieurs actions sont définies dans le
dossier reducers, situé dans le dossier src

"""