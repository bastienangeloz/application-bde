# Application MyBDE
___

### Stack technique
**Version : Python 3.8.10**
> - Base de donnée : POSTGRESSQL (SQL)
> - Framework Back-end : Django (Python)
> - Framework Front-end : React (JS/JSX)
> - Store Front-end : Redux (JS/JSX)
> - Queuing : Celery (Python)
> - Transporteur : Reddis (Python)
> - Cache: Reddis (Python)
> - Paiement: API stripe (Python)

## Install

Créer un environnement virtuel
```
virtualenv .venv
```
Activer l'environnement
Windows :
```
.venv\\Scripts\\activate
```
Mac / Linux :
```
source .venv/bin/activate
```
Installer les dependance backend
```
pip install -r requirements.txt
```

Instaler les dependance frontend

Vous devez installer Node.js pour travailler sur le frontend
```
cd/frontend
```

```
npm install
```

## Run

Lancer le serveur en local depuis la racine du projet
```
py manage.py runserver
```
Pour le developpement frontend, lancer le compilateur à partir du dossier frontend
```
npm run dev
```

## Test

Lancement des tests unitaires avec coverage
```
coverage manage.py test
```
build resultat des test sous format html
```
coverage html
```
Lancement des tests de mise en charge avec locust
```
locust -f performance_test.py --host http://localhost:8000
```

___

## Maintenabilité et migrations

Pour une optimisation des migrations et des montées de versions, nous utiliserons le minimum de bibliotheque tierce possible, et si utilisé, nous ferons le choix de bibliotheque très représenté dans la communauté avec un haut niveau d'engagement et de maintenabilité.

Liste des bibliothèques et versions **principales** utilisés **coté back-end**.

Elles ont des sous-dependances installées.

La liste complete est dans le fichier **requirements.txt**
```
celery==5.2.7
celery-progress==0.1.3
coverage==7.1.0
Django==4.1.5
django-celery-beat==2.4.0
django-celery-results==2.4.0
django-jazzmin==2.6.0
djangorestframework==3.14.0
djangorestframework-simplejwt==4.8.0
django-redis==5.2.0
djoser==2.1.0
numpy==1.24.1
postgres==4.0
psycopg2==2.9.5
python-decouple==3.7
redis==4.5.1
scikit-learn==1.2.1
scipy==1.10.0
social-auth-app-django==4.0.0
social-auth-core==4.3.0
stripe==5.1.1
whitenoise==6.3.0
```

Liste des bibliothèque et versions necessaire au **developpement** utilisés coté **front-end**.
```
"devDependencies": {
    "@babel/plugin-proposal-class-properties": "^7.18.6",
    "@babel/preset-env": "^7.20.2",
    "@babel/preset-react": "^7.18.6",
    "@emotion/react": "^11.10.5",
    "@emotion/styled": "^11.10.5",
    "@testing-library/jest-dom": "^5.16.5",
    "@testing-library/react": "^13.4.0",
    "@testing-library/user-event": "^13.5.0",
    "babel-plugin-transform-runtime": "^6.23.0",
    "css-loader": "^6.7.3",
    "sass": "^1.57.1",
    "sass-loader": "^13.2.0",
    "webpack": "^5.75.0",
    "webpack-cli": "^5.0.1"
}
```

Liste des bibliothèque et versions necessaire **au projet** utilisés coté **front-end**.
```
"dependencies": {
    "@mui/material": "^5.11.5",
    "@reduxjs/toolkit": "^1.9.2",
    "@stripe/react-stripe-js": "^1.16.4",
    "@stripe/stripe-js": "^1.46.0",
    "ag-grid-community": "^29.0.0",
    "ag-grid-enterprise": "^29.0.0",
    "ag-grid-react": "^29.0.0",
    "axios": "^1.2.6",
    "js-cookie": "^3.0.1",
    "papaparse": "^5.3.2",
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "react-icons": "^4.7.1",
    "react-burger-menu": "^3.0.9",
    "react-intl": "^6.2.5",
    "react-modal": "^3.16.1",
    "react-player": "^2.11.2",
    "react-redux": "^8.0.5",
    "react-router-dom": "^6.6.2",
    "react-scripts": "^5.0.1",
    "react-select": "^5.7.0",
    "react-switch": "^7.0.0",
    "react-tabs": "^6.0.0",
    "react-toastify": "^9.1.1",
    "recharts": "^2.3.2",
    "redux": "^4.2.1",
    "redux-devtools-extension": "^2.13.9",
    "redux-thunk": "^2.4.2",
    "styled-components": "^5.3.6",
  }
```


## TODO

###### Backend

###### Frontend

###### Post-Dev
